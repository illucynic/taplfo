# TAPLFO

TAPLFO is a fully featured voltage controlled LFO Kosmo module based on the Electric Druid chip of [the same name](https://electricdruid.net/product/taplfo3/). The main feature of this LFO is that its frequency can be set by tapping the button or synced from an external clock or trigger source. It can produce 16 waveforms between two banks of 8, has voltage control over all parameters, and has both unipolar (0-10V) and bipolar (±5V) outputs.

## Version

The current version of this module is v1.0. I have built a prototype version of this module and everything works as far as I can tell. I made some changes for v1.0 and have not built this specific version yet, but in theory it _should_ work.

## Specs

Width: 10 cm  
Depth: TBD cm  
Supply voltage: ±12V<sub>DC</sub>, via 10-pin IDC header  
Current: TBD

## Gerbers

The gerber files were generated with the intent of being fabricated by JLCPCB. As such, they were created with JLCPCB's recommendations in mind, notably using the Protel filename extensions and the inclusion of "JLCJLCJLCJLC" on all boards. Other fabricators may have different recommendations and requirements.

## Photos

Front:  
![](images/taplfo_front.jpg)

Back:  
![](images/taplfo_back.jpg)

## Documentation

* [Schematic](schematic/taplfo_schematic.pdf)
* [BOM](bom/TAPLFO v1.0_bom.md)
* [Interactive BOM](bom/TAPLFO v1.0 Interactive BOM.html) (Please note that off-board components [rotary switch, pushbutton, etc.] are not included on the interactive BOM.)

## GitLab repository

* [https://gitlab.com/illucynic/taplfo](https://gitlab.com/illucynic/taplfo)