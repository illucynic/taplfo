# TAPLFO.kicad_sch BOM

11/18/2023 12:15:19 PM

Generated from schematic by Eeschema 7.0.1

**Component Count:** 115

| Refs | Qty | Component | Description |
| ----- | --- | ---- | ----------- |
| C1, C2 | 2 | 10uF | Polarized capacitor, US symbol |
| C3–10, C12 | 9 | 100nF | Unpolarized capacitor |
| C11 | 1 | 220nF | Unpolarized capacitor |
| C13, C14, C16, C17 | 4 | 100nF | Unpolarized capacitor |
| C15 | 1 | 33nF | Unpolarized capacitor |
| C18, C20 | 2 | 10nF | Unpolarized capacitor |
| C19, C21 | 2 | 2.2nF | Unpolarized capacitor |
| D1, D2, D5–14 | 12 | 1N5817 | 20V 1A Schottky Barrier Rectifier Diode, DO-41 |
| D3, D4 | 2 | 1N4148 | 100V 0.15A standard switching diode, DO-35 |
| D15 | 1 | Bi-color LED | Dual LED, bidirectional |
| D16 | 1 | LED | Light emitting diode |
| J1 | 1 | Power_2x5 | Generic connector, double row, 02x05, odd/even pin numbering scheme (row 1 odd numbers, row 2 even numbers), script generated (kicad-library-utils/schlib/autogen/connector/) |
| J2 | 1 | Sync Input | 1/4" Audio Jack, 2 Poles (Mono / TS) |
| J3 | 1 | Waveform Set CV | 1/4" Audio Jack, 2 Poles (Mono / TS) |
| J4 | 1 | Conn_01x03_Pin | Generic connector, single row, 01x03, script generated |
| J5 | 1 | Waveform CV | 1/4" Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) |
| J6 | 1 | Frequency CV | 1/4" Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) |
| J7 | 1 | Multiply CV | 1/4" Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) |
| J8 | 1 | Level CV | 1/4" Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) |
| J9 | 1 | Distort CV | 1/4" Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) |
| J10 | 1 | Unipolar Output | 1/4" Audio Jack, 2 Poles (Mono / TS) |
| J11 | 1 | Bipolar Output | 1/4" Audio Jack, 2 Poles (Mono / TS) |
| J12 | 1 | Clock Output | 1/4" Audio Jack, 2 Poles (Mono / TS) |
| J13 | 1 | 3_pin_Molex_header | Generic connector, single row, 01x03, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J14 | 1 | 3_pin_Molex_connector | Generic connector, single row, 01x03, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J15 | 1 | Conn_01x09_Socket | Generic connector, single row, 01x09, script generated |
| J16 | 1 | Conn_01x09_Pin | Generic connector, single row, 01x09, script generated |
| J17 | 1 | Conn_01x06_Socket | Generic connector, single row, 01x06, script generated |
| J18 | 1 | Conn_01x06_Pin | Generic connector, single row, 01x06, script generated |
| Q1–3 | 3 | BC547 | 0.1A Ic, 45V Vce, Small Signal NPN Transistor, TO-92 |
| R1, R2, R4–10, R12–14, R17, R18, R22, R28–30 | 18 | 10k | Resistor, US symbol |
| R3, R11, R38–41 | 6 | 4.7k | Resistor, US symbol |
| R15, R19, R20 | 3 | 12k | Resistor, US symbol |
| R16 | 1 | 33k | Resistor, US symbol |
| R21 | 1 | 47k | Resistor, US symbol |
| R23 | 1 | 20k | Resistor, US symbol |
| R24 | 1 | 24k | Resistor, US symbol |
| R25, R31, R33, R35 | 4 | 1k | Resistor, US symbol |
| R26, R27, R34, R36, R37 | 5 | 100k | Resistor, US symbol |
| R32 | 1 | 1K | Resistor, US symbol |
| RV1–4 | 4 | B100k | Potentiometer, US symbol |
| RV5 | 1 | 100k | Trim-potentiometer, US symbol |
| SW1 | 1 | Tap Tempo | Push button switch, generic, two pins, Tayda #A-4722 |
| SW2 | 1 | Waveform Set | Switch, single pole double throw |
| SW3 | 1 | SW_Rotary12 | rotary switch with 12 positions, Tayda #A-1893 |
| U1 | 1 | TAPLFO3 | Electric Druid TAPLFO chip |
| U2 | 1 | L7805 | Positive 1.5A 35V Linear Regulator, Fixed Output 5V, TO-220/TO-263/TO-252 |
| U3 | 1 | TL074 | Quad Low-Noise JFET-Input Operational Amplifiers, DIP-14/SOIC-14 |
| U4 | 1 | TL072 | Dual Low-Noise JFET-Input Operational Amplifiers, DIP-8/SOIC-8 |
| U5–7 | 3 | MCP6002 | 1MHz, Low-Power Op Amp, DIP-8 |

